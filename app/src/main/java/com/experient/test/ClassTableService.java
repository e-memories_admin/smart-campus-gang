package com.experient.test;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.IBinder;
import android.widget.RemoteViewsService;

public class ClassTableService extends RemoteViewsService {

    @SuppressLint("Range")
    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);

    }

    /**
     * 循环获取数据并更新
     * @param intent
     * @return
     */
    @SuppressLint("Range")
    @Override
    public RemoteViewsService.RemoteViewsFactory onGetViewFactory(Intent intent) {

        return new TableRemoteViewFactory(this.getApplicationContext(), intent);
    }
}
