package com.experient.test;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import io.dcloud.WebAppActivity;

/**
 * @author w1760
 */
public class MainActivity extends WebAppActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public boolean isStreamAppMode() {
        return false;
    }

}