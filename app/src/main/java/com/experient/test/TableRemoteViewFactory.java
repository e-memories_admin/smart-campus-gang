package com.experient.test;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import com.experient.test.struct.Table;

import java.util.ArrayList;
import java.util.List;

/**
 * 小部件ListView适配器
 * @author 王宇哲
 */
public class TableRemoteViewFactory implements RemoteViewsService.RemoteViewsFactory {

    private final Context mContext;
    private final int myAppWidgetId;
    private static List<Table> mTableList;
    private static final int[] backGroups = new int[]{
            R.drawable.split_line_blue,
            R.drawable.split_line_green,
            R.drawable.split_line_red,
            R.drawable.split_line_teal,
    };


    public TableRemoteViewFactory(Context context, Intent intent) {

        mTableList = new ArrayList<>();
        mContext = context;
        myAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);

    }

    public static void refresh(List<Table> tableList){


        if (mTableList != null) {
            mTableList.clear();
        }else {
            mTableList = new ArrayList<>();
        }




        mTableList.addAll(tableList);
    }


    @Override
    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public int getCount() {
        return mTableList.size();
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews remoteViews = new RemoteViews(mContext.getPackageName(), R.layout.list_content);
        Table table = mTableList.get(position);
        remoteViews.setTextViewText(R.id.fromTimeText, table.getFromTime());
        remoteViews.setTextViewText(R.id.toTimeText, table.getToTime());
        remoteViews.setTextViewText(R.id.className, table.getClassName());

        //设置背景
        remoteViews.setInt(R.id.oneLine, "setBackgroundResource", backGroups[position % backGroups.length]);

        String detail = table.getSection()+" | "+table.getRoomName()+" | "+table.getTeacherName();
        remoteViews.setTextViewText(R.id.classDetail, detail);

        return remoteViews;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}
