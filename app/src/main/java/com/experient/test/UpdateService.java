package com.experient.test;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.util.Log;
import androidx.annotation.Nullable;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author w1760
 */
public class UpdateService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @SuppressLint("Range")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //创建线程池来获取数据
        ThreadFactory threadFactory = Thread::new;
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), threadFactory);
        threadPoolExecutor.execute(() -> {
            while (true) {
                try {
                    //发送广播来更新页面
                    Intent intent1 = new Intent("TABLE_UPDATE_ACTION");
                    sendBroadcast(intent1);

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {
                        Thread.sleep(3*60*60*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });

        return super.onStartCommand(intent, flags, startId);
    }

}