package com.experient.test.api;

import android.accounts.NetworkErrorException;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 王宇哲
 * @date 2022/5/3
 * 通过接口获取课程表信息
 */
public class GetTableList {
    private static final String URL = "https://school.e-memories.cn/api/Android/getTodayTable";
    public static Map getTableData(String SafeCode) throws NetworkErrorException, JSONException {
        Map<String,String> params = new HashMap<>();

        params.put("SafeCode",SafeCode);

        //获取课程表信息
        JSONObject jsonObject = UrlRequest.post(URL,params);

        Map<String,Object> tableData = new HashMap<>();

        //获取周数
        if(jsonObject.has("week")){
            tableData.put("week",jsonObject.getString("week"));
        }
        //获取星期
        if(jsonObject.has("day")){
            tableData.put("day",jsonObject.getString("day"));
        }
        //获取课程表
        if(jsonObject.has("data")){
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            List<Object> classTable = new ArrayList<>();
            //获取课程信息的键值对
            for(int i = 0;i<jsonArray.length();i++){
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                //获取当前课程的信息
                Map<String,Object> classInfo = new HashMap<>();
                if(jsonObject1.has("subject")){
                    classInfo.put("subject",jsonObject1.getString("subject"));
                }
                if(jsonObject1.has("teacher")){
                    classInfo.put("teacher",jsonObject1.getString("teacher"));
                }
                if(jsonObject1.has("classroom")){
                    classInfo.put("classroom",jsonObject1.getString("classroom"));
                }
                if(jsonObject1.has("fromTime")){
                    classInfo.put("fromTime",jsonObject1.getString("fromTime"));
                }
                if(jsonObject1.has("toTime")){
                    classInfo.put("toTime",jsonObject1.getString("toTime"));
                }
                if(jsonObject1.has("section")){
                    classInfo.put("section",jsonObject1.getString("section"));
                }

                classTable.add(classInfo);
            }
            tableData.put("classTable",classTable);
        }


        return tableData;
    }
}
