package com.experient.test.struct;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 课程表类
 * @author 王宇哲
 * 存储课程信息
 */
public class Table implements Parcelable, Comparable<Table> {
    private String fromTime;
    private String toTime;
    private String className;
    private String teacherName;
    private String roomName;
    private String section;

    public Table(String fromTime, String toTime, String className, String teacherName, String roomName, String section) {
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.className = className;
        this.teacherName = teacherName;
        this.roomName = roomName;
        this.section = section;
    }

    protected Table(Parcel in) {
        fromTime = in.readString();
        toTime = in.readString();
        className = in.readString();
        teacherName = in.readString();
        roomName = in.readString();
        section = in.readString();
    }

    public Table() {}

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    /**
     * 获取开始时间的整数形式
     * @return
     */
    public Integer getFromTimeInt() {
        return getTimeStamp(fromTime);
    }

    /**
     * 获取结束时间的整数形式
     * @return
     */
    public Integer getToTimeInt() {
        return getTimeStamp(toTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public String toString() {
        return "Table{" +
                "fromTime='" + fromTime + '\'' +
                ", toTime='" + toTime + '\'' +
                ", className='" + className + '\'' +
                ", teacherName='" + teacherName + '\'' +
                ", roomName='" + roomName + '\'' +
                ", section='" + section + '\'' +
                '}';
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(fromTime);
        parcel.writeString(toTime);
        parcel.writeString(className);
        parcel.writeString(teacherName);
        parcel.writeString(roomName);
        parcel.writeString(section);
    }




    public static final Creator<Table> CREATOR = new Creator<Table>() {
        @Override
        public Table createFromParcel(Parcel in) {
            return new Table(in);
        }

        @Override
        public Table[] newArray(int size) {
            return new Table[size];
        }
    };

    /**
     * 实现比较方法，用于排序
     * @param table 另一个课程表
     * @return 比较结果
     */
    @Override
    public int compareTo(Table table) {

        //比较时间，我们按照时间升序排序
        String tableTime = table.getToTime();
        String time = this.getToTime();

        //将时间字符串转换为秒数
        int tableTimeStamp = getTimeStamp(tableTime);
        int timeStamp = getTimeStamp(time);

        //比较结果
        return -Integer.compare(tableTimeStamp, timeStamp);
    }


    private int getTimeStamp(String time){
        String[] timeArray = time.split(":");

        if (timeArray.length == 2){
            int hour = Integer.parseInt(timeArray[0]);
            int minute = Integer.parseInt(timeArray[1]);
            return (hour * 60 + minute)*60;
        }else{
            int hour = Integer.parseInt(timeArray[0]);
            int minute = Integer.parseInt(timeArray[1]);
            int second = Integer.parseInt(timeArray[2]);
            return hour * 3600 + minute*60 + second;
        }

    }


}
